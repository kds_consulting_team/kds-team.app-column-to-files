'''
Created on 10. 10. 2018

@author: daivd esner
'''

from kbc.env_handler import KBCEnvHandler
import logging
import os
import pandas as pd

KEY_COL_NAMES = 'col_names'
KEY_NAME = 'col_name'
KEY_SRC_FILE = 'src_table'
KEY_RES_PREFIX = 'prefix'
KEY_RES_PREFIX_col = 'prefix_col'
KEY_RES_EXTENSION = 'res_extension'

MAX_CHUNK_SIZE = 10 ** 6

MANDATORY_PARS = [KEY_COL_NAMES]

APP_VERSION = '0.0.3'


class Component(KBCEnvHandler):

    def __init__(self, debug=False):
        KBCEnvHandler.__init__(self, MANDATORY_PARS)
        # override debug from config
        if(self.cfg_params.get('debug')):
            debug = True

        self.set_default_logger('DEBUG' if debug else 'INFO')
        logging.info('Running version %s', APP_VERSION)
        logging.info('Loading configuration...')

        try:
            self.validateConfig()
        except ValueError as e:
            logging.error(e)
            exit(1)

    def run(self, debug=False):
        '''
        Main execution code
        '''
        params = self.cfg_params  # noqa

        for col in params[KEY_COL_NAMES]:

            table = self.get_input_table_by_name(col.get(KEY_SRC_FILE))
            if not table:
                logging.error(
                    'Source table %s does not exist in input mapping', col.get(KEY_SRC_FILE))
                exit(1)
            logging.info("Converting col %s from table %s",
                         col[KEY_NAME], table['destination'])

            try:
                self._process_in_chunks(table, col)
            except Exception as e:
                logging.error(e)
                exit(1)

        logging.info("Conversion finished sucessfully.")

    def _process_in_chunks(self, table, col_config):
        for chunk in pd.read_csv(table['full_path'], chunksize=MAX_CHUNK_SIZE, dtype=str):

            if not col_config[KEY_NAME] in chunk.columns:
                raise ValueError(
                    'No column [%s] found in input table %s', col_config[KEY_NAME], table['full_path'])
            if col_config.get(KEY_RES_PREFIX_col) and not col_config[KEY_RES_PREFIX_col] in chunk.columns:
                raise ValueError('No prefix column [%s] found in input table %s',
                                 col_config[KEY_RES_PREFIX_col], table['full_path'])
            self._save_to_file(chunk, table['destination'], col_config[KEY_NAME], col_config.get(KEY_RES_PREFIX),
                               col_config.get(KEY_RES_PREFIX_col), col_config.get(KEY_RES_EXTENSION))

    def _save_to_file(self, data, table_name, col_name, prefix, prefix_col, extension):
        res_files = []
        for index, row in data.iterrows():
            res_name = str(index) + '_' + table_name + extension
            if prefix_col:
                res_name = row[prefix_col] + '_' + res_name
            if prefix:
                res_name = prefix + '_' + res_name
            res_path = os.path.join(self.data_path, 'out', 'files', res_name)
            with open(res_path, 'w+') as res_file:
                res_file.write(row[col_name])
            res_files.append(res_path)
        return res_files


"""
    Main entrypoint
"""
if __name__ == "__main__":
    comp = Component()
    comp.run()
