# Convert Column values to files

Simple APP that takes specified column values from single input table and stores them in `out/files/` folder. Currently this is only to use in conjunction with other processors like [XML2CSV](https://bitbucket.org/kds_consulting_team/kds-team.processor-xml2csv/src) or [JSON2CSV](https://bitbucket.org/kds_consulting_team/kds-team.processor-json2csv/src) since it does not produce file manifests, hence does not store any files in filestorage as a result.



## Usage

- **Source table name** (REQ) - source table name as specified in input mapping.
- **Column name** (REQ) - Column that is to be converted
- **Result file prefix col** (OPTIONAL) - Name of the secondary column whose value is used as a prefix of the result file. The result files will be then outputed in this form: `[PREFIX_COL_VALUE]_[INDEX]_[TABLE_NAME].[EXTENSION]`
- **Result file prefix** (OPTIONAL) - Prefix that is added to the result file name. The result files will be then outputed in this form: `[FILE_PREFIX]_*[PREFIX_COL_VALUE]*_[INDEX]_[TABLE_NAME].[EXTENSION]`
- **Extension of result file** (OPTIONAL) - Optional extension of the result files.


### Example
Let `input.csv` be input mapping of table with colums `[VALUE,INDEX_COL]` and 2 rows:

| VALUE | INDEX_COL
|--|--|
| VAL1 | index1|
| VAL2 | index2|

Following configuration:

**Source table name** : `input.csv`;
**Column name** : `VALUE`
**Result file prefix col** : `INDEX_COL`
**Extension of result file** : `xml`

Will result in 2 files stored in the `out/files/` folder:

 - `index1_1_input.csv.xml`
 - `index2_2_input.csv.xml`

Containing the values of the `VALUE` column respectively.