**0.0.5**

- KBC functional tests
  
**0.0.4**

- bugfixes fixes
- working prefix based on col value

**0.0.3**

- add dependency to base lib
- basic tests

**0.0.1**

- add utils scripts
- move kbc tests directly to pipelines file
- use uptodate base docker image
- add changelog
